(ns hwo2014bot.communication
  (:require [clojure.data.json :as json])
    (:use [aleph.tcp :only [tcp-client]]
          [lamina.core :only [enqueue wait-for-result wait-for-message]]
          [gloss.core :only [string]]))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))
