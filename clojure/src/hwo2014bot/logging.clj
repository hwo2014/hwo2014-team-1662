(ns hwo2014bot.logging
  (:use [clojure.pprint]))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    (pprint msg)))

(defn log-sensors [msg]
  (pprint msg))