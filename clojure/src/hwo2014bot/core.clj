(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [gloss.core :only [string]]
        [clojure.pprint]
        [hwo2014bot.logging]
        [hwo2014bot.communication])
  (:gen-class))

(def track (atom []))

(def distance-travelled-sensor (atom []))

(def distance-to-next-corner-sensor (atom []))

(def speed-sensor (atom []))

(def acceleration-sensor (atom []))

(def best-corner (atom {:speed 0 :angle nil :radius nil}))

(def bot-name (atom ""))


(def not-nil? (complement nil?))

(defn get-bot-info [data]
  (first (filter #(= @bot-name (get-in % [:id :name])) data)))

(defn piece-type? [piece]
  (if (not-nil? (:radius piece))
    :corner
    :straight))

(defn length-of-piece [piece]
  (if (= (piece-type? piece) :straight)
    (:length piece)
    (* (/ (. Math abs (:angle piece)) 360) (. Math PI) 2 (:radius piece))))

(defn distance-to-next-corner
  ([track]
  (let [piece (first track)]
    (if (= (piece-type? piece) :corner)
      0
      (+ (:length piece) (distance-to-next-corner (rest track))))))

  ([position track]
    (let [piece (nth track (:pieceIndex position))
          distance-end-piece (- (length-of-piece piece) (:inPieceDistance position))]
      (if (= :corner (piece-type? piece))
        0
        (+ distance-end-piece (distance-to-next-corner (drop (+ (:pieceIndex position) 1) (cycle track))))))))

(defn track-distance [track]
  (reduce + (map length-of-piece track)))

(defn distance-travelled [position track]
  (let [track-covered (take (:pieceIndex position) track)]
    (+ (:inPieceDistance position)
       (* (:lap position) (track-distance track))
       (track-distance track-covered))))

(defn diff
  "differences a sequence"
  [values]
  (if (< (count values) 2)
    nil
    (cons (- (first values) (second values)) (diff (rest values)))))


(def ping-msg {:msgType "ping" :data "ping"})

(defn sensors-message
  "constructs a message for performance tracking"
  []
  {
    :distance-travelled (first @distance-travelled-sensor)
    :distance-to-next-corner (first @distance-to-next-corner-sensor)
    :speed (first @speed-sensor)
    :acceleration (first @acceleration-sensor)
  })

(defn update-distance-travelled [position track]
  (swap! distance-travelled-sensor
         #(cons (distance-travelled position track) %)))

(defn update-distance-to-next-corner [position track]
  (swap! distance-to-next-corner-sensor
         #(cons (distance-to-next-corner position track) %)))

(defn update-speed [distances]
  (reset! speed-sensor (diff distances)))

(defn update-acceleration [speeds]
  (reset! acceleration-sensor (diff speeds)))

(defn update-best-corner [position track angle speeds max-corner-speed]
  (let [piece (nth track (:pieceIndex position))]
    (if (and (= (piece-type? piece) :corner) (> (first speeds) max-corner-speed))
        (reset! best-corner
                {:speed (first speeds) :angle angle :radius (:radius piece)}))))


(defmulti get-throttle (fn [regime & args] regime))

(defmethod get-throttle "speed-up-on-straights" [_ piece-index next-piece-index]
  (if (= :straight (piece-type? (nth @track piece-index)) (piece-type? (nth @track next-piece-index)))
    {:msgType "throttle" :data 1.0}
    {:msgType "throttle" :data 0.5}))

(defmethod get-throttle "constant-throttle" [_ throttle]
  {:msgType "throttle" :data throttle})


(defmulti handle-msg :msgType)

(defmethod handle-msg "carPositions" [msg]
  (let [me (get-bot-info (:data msg))
        piece-index (get-in me [:piecePosition :pieceIndex])
        next-piece-index (mod (inc piece-index) (count @track))]

    ;; the updating of the speed/distance sensors could be done better since
    ;; the distance has to be updated before the speed sensor
    (update-distance-travelled (:piecePosition me) @track)
    (update-distance-to-next-corner (:piecePosition me) @track)
    (update-speed @distance-travelled-sensor)
    (update-acceleration @speed-sensor)
    (update-best-corner (:piecePosition me) @track (:angle me) @speed-sensor (:speed @best-corner))
    (log-sensors (sensors-message))

    (get-throttle "constant-throttle" 0.6)))

(defmethod handle-msg "gameInit" [msg]
  (let [t (get-in msg [:data :race :track :pieces])]
    (reset! track t)
    ping-msg))

(defmethod handle-msg :default [msg]
  ping-msg)

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (reset! bot-name botname)
    (game-loop channel)))
